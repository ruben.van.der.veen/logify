import { all, call } from 'redux-saga/effects'
import dashboard from './Dashboard/saga'
import availability from './Availability/saga'

export default function* rootSaga() {
  yield all([call(dashboard), call(availability)])
}
