import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import Resizer from '../components/Resizer'
import { ScrollView, StyleSheet, View } from 'react-native'
import { useSelector } from 'react-redux'
import { Button, Text, Title } from 'react-native-paper'
import { useFirebase } from 'react-redux-firebase'
import { switchMenu } from '../Navigation.web'

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

function Account() {
  const firebase = useFirebase()
  const auth = useSelector((state: any) => state.firebase.auth)

  const __doLogout = () => {
    firebase.logout().then(() => switchMenu('/auth'))
  }

  return (
    <ScrollView style={{ margin: 16, height: '100%' }}>
      <Resizer>
        <>
          <View style={styles.header}>
            <Title>My account</Title>
            <Button onPress={__doLogout}>Logout</Button>
          </View>
          <View>
            <Text>My email: {auth.email}</Text>
          </View>
        </>
      </Resizer>
    </ScrollView>
  )
}

export default safeAreaHOC(Account)
