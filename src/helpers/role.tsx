// @param user the user to check
// @param expectedRole = the specified role you're checking
// @return Returns true if user has the role of an advisor
import { AuthUserType } from '../reducers'

export const hasRole = (
  user: AuthUserType | { roles: any },
  expectedRole: string
) => {
  return (user?.roles || []).some(
    (role: { name: string }) => role.name === expectedRole
  )
}
