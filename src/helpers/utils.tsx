export const isEmpty = (str: string) => !str || str.length === 0
export const uppercaseFirstLetter = (str: string) =>
  str.charAt(0).toUpperCase() + str.slice(1)
export const validateEmail = (email: string) =>
  /\S+@\S+\.\S+/.test(email) && email.includes('@') && email.includes('.')
export const getFormData = (object: { [key: string]: any }) => {
  const formData = new FormData()
  Object.keys(object).forEach((key) => formData.append(key, object[key]))
  return formData
}
export const errorObject = (key: string, message: string) => ({
  key,
  message,
})

export const validate = (value: any, length: number) => {
  if (!value || value.length < length) {
    return false
  }
  return true
}

export const requiredValidate = (
  fields: { [key: string]: any },
  values: any[],
  key: string,
  value: string
) => {
  if (!validate(value, 1)) {
    return errorObject(
      key,
      fields && fields[key] && fields[key].label
        ? `${fields[key].label(fields, values, key, value)} is verplicht.`
        : 'Veld is verplicht'
    )
  }
  return null
}

export function secondsToDDMMYYYY(seconds: number) {
  const date = new Date(seconds * 1000)
  const day = ((d) => (d > 9 ? d : `0${d}`))(date.getDate())
  const month = ((m) => (m > 9 ? m : `0${m}`))(date.getMonth() + 1)
  const year = date.getFullYear()
  return `${day}-${month}-${year}`
}
