import { call, put, select, take, takeLatest } from 'redux-saga/effects'
import api from 'react-api'
import 'firebase/compat/database'

export const numberPerRequest = 200
// DISABLE THIS IN PROD, NECESSARY FOR users in test
// export const numberPerRequest = Platform.OS === 'web' ? 50 : 100

function* load(actions, getPath, getState, action) {
  try {
    const state = yield select((state) => state)
    const path = getPath(state)
    const { crud } = yield select((state) => getState(state))
    const { object } = crud
    if ((!object || !object.id) && !action.payload.id) {
      yield put(actions.loaded(object))
      return
    }

    const { params, fetchOptions, cachedFromList } = action.meta || {}
    if (cachedFromList) {
      yield put(actions.loaded(object))
      // return
    }
    const id = object.id ? object.id : action.payload.id
    const response = yield call(api, {
      path: `${path}/${id}`,
      params,
      ...fetchOptions,
    })

    yield put(actions.loaded(response))
  } catch (e) {
    yield put(actions.loaded(e, null, true))
  }
}

function* loadList(actions, getPath, getState, action, loadMore) {
  try {
    const state = yield select((state) => state)
    const url = getPath(state)
    const { list } = yield select((state) => getState(state))
    // const resp = firebase.list('/availability')
    // console.log(resp)
    const meta = action.meta || {}
    let status
    if (action && action.payload && action.payload.status) {
      status = action.payload.status
    }
    let params = status
      ? {
          ...list.params,
          ...meta.params,
          status,
        }
      : {
          ...list.params,
          ...meta.params,
        }

    if (loadMore) {
      const { offset, total } = list
      if (offset > total) {
        return
      }
      params.offset = (offset || 0) + numberPerRequest
    }
    const response = yield call(api, {
      method: 'GET',
      path: url,
      params,
    })
    yield put(
      loadMore
        ? actions.loadedMoreList(response, meta)
        : actions.loadedList(response, meta)
    )
  } catch (e) {
    yield put(
      loadMore
        ? actions.loadedMoreList(e, null, true)
        : actions.loadedList(e, null, true)
    )
  }
  if (actions.LOAD_MORE_LIST) {
    yield take(actions.LOAD_MORE_LIST)
    yield call(loadList, actions, getPath, getState, action, true)
  }
}

function* update(actions, getPath, getState, action) {
  try {
    const state = yield select((state) => state)
    const path = getPath(state)
    const { crud } = yield select((state) => getState(state))
    const { object } = crud
    const { params } = action.meta || {}
    let fullBody = object
    if (action && action.payload && action.payload.id) {
      fullBody = { ...fullBody, ...action.payload }
    }

    const response = yield call(api, {
      path: `${path}/${object.id}`,
      method: 'PUT',
      body: fullBody,
      params,
    })
    yield put(actions.updated(response))
  } catch (e) {
    yield put(actions.updated(e, null, true))
  }
}

function* remove(actions, getPath, getState, action) {
  try {
    const state = yield select((state) => state)
    const path = getPath(state)
    const { crud } = yield select((state) => getState(state))
    const { object } = crud
    const { params, id } = action.meta || {}
    // const response =
    if (object && object.id) {
      yield call(api, {
        path: `${path}/${object.id}`,
        method: 'DELETE',
        params,
      })
    } else if (id) {
      yield call(api, {
        path: `${path}/${id}`,
        method: 'DELETE',
        params,
      })
    } else {
      yield call(api, {
        path: `${path}`,
        method: 'DELETE',
        params,
        body: action.payload,
      })
    }

    yield put(actions.removed(object))
  } catch (e) {
    console.log(e)
    yield put(actions.removed(e, null, true))
  }
}

function* create(actions, getPath, getState, action) {
  try {
    const state = yield select((state) => state)
    const url = getPath(state)
    const { crud } = yield select((state) => getState(state))
    const { object } = crud
    const { params, extraBody } = action.meta || {}

    let body =
      extraBody && extraBody.dontAddObject
        ? extraBody
        : { ...object, ...extraBody }

    let response = yield call(api, {
      path: url,
      method: 'POST',
      body,
      params,
    })

    yield put(actions.created(response))
  } catch (e) {
    yield put(actions.created(e, null, true))
  }
}

export default function* pagedList(
  actions,
  getPath = (state) => '',
  getState = (state) => console.log('get state not implemented'),
  reloadConfig
) {
  let reloadActions = [actions.CREATED, actions.UPDATED, actions.REMOVED]
  if (reloadConfig && reloadConfig.removeOthers) {
    reloadActions = reloadConfig.reloadActions
  } else if (reloadConfig) {
    reloadActions = [...reloadConfig.reloadActions, reloadActions]
  }

  yield takeLatest(
    [actions.LOAD_LIST, actions.RELOAD_LIST, ...reloadActions].filter((n) => n),
    loadList,
    actions,
    getPath,
    getState
  )

  yield takeLatest(actions.LOAD, load, actions, getPath, getState)
  yield takeLatest(actions.UPDATE, update, actions, getPath, getState)
  yield takeLatest(actions.CREATE, create, actions, getPath, getState)
  yield takeLatest(actions.REMOVE, remove, actions, getPath, getState)
}
