import { combineReducers, Reducer } from 'redux'

import widthAndHeight from './WidthAndHeight/reducer'
import dashboard from './Dashboard/reducer'
import notification from './Notifications/reducer'
import availability from './Availability/reducer'
import { firebaseReducer } from 'react-redux-firebase'
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import { firestoreReducer } from 'redux-firestore' // <- needed if using firestore
import 'firebase/compat/firestore'

const fbConfig = {
  apiKey: 'AIzaSyDIuNSYR4NYcYmExV_bb5sN31qnIsPlfKo',
  authDomain: 'logify-9b345.firebaseapp.com',
  databaseURL:
    'https://logify-9b345-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'logify-9b345',
  storageBucket: 'logify-9b345.appspot.com',
  messagingSenderId: '242015076061',
  appId: '1:242015076061:web:377a33b0e9e3f165546534',
  measurementId: 'G-DMKPKZCD40',
}

firebase.initializeApp(fbConfig)
firebase.firestore()
const rootReducer = combineReducers({
  notification,
  dashboard,
  widthAndHeight,
  availability,
  firebase: firebaseReducer,
  firestore: firestoreReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export type AttributeKey = string
export type AttributeType = {
  id?: number
  key: AttributeKey
  value: any
  type: 'string' | 'media' | 'attributes' | 'entity' | 'boolean' | 'integer'
}

export type AddressType = {
  id: undefined | number
  type: 'home' | 'work' | 'undefined'
  street: string
  houseNumber: string
  city: string
  postalCode: string
  latitude: number
  longitude: number
  country: string
  province?: string
}

export type UserType = {
  addresses: AddressType[] | null
  phone_numbers: [] | null
  attributes: AttributeType[] | null
  gender: 'U'
  initials: string
  first_name: string
  preposition: string
  last_name: string
  email: string
  company: { name: string }
  sepa: string
  externalId: string
  id: number
  createdAt: number
  updatedAt: number
  organization_id: number
  rate: number
  billable: boolean
  language: string
  status: string
  function: string
}
export type OrganizationType = {
  id: number
  name: string
  email: string
  phone: string
  settings: { realTimePrice?: string; enableCenterBlock?: string } | null
  addresses: AddressType[] | null
  identificationNumber: string
  vatNumber: string
  ibanNumber: string
  logoUrl: string
  organizationGroups: null
  status: string
  website: string
  invoiceEmail: string
  createdAt: number
  updatedAt: number
}
export type AuthUserType = {
  user_id: number
  roles: { name: string }[] | null
} & UserType

export type AuthStateType = {
  restoring: boolean
  restoringError: boolean
  restored: boolean
  refreshing: boolean
  updating: boolean
  loading: boolean
  refreshingError: boolean
  tokens: {
    idToken: string
    accessToken: string
    refreshToken: string
  }
  user: {
    user: AuthUserType | null
    organization: OrganizationType | null
  } | null
  authenticating: boolean
  authenticatingError: boolean
  authenticated: boolean
  tokenValidated: boolean
  tokenValidating: boolean
  tokenValidationError: boolean
  loginStatus: 'logged_in'
  accountCompleted: boolean
}
export type AuthReducerType = Reducer<AuthStateType>

export type RegisterStateType = {
  data: {}
  registering: boolean
  registered: boolean
  sending: boolean
  error: boolean
}
export type RegisterReducerType = Reducer<RegisterStateType>
export type ForgotPasswordStateType = {
  requesting: boolean
  requestingError: boolean
  requested: boolean
  changing: boolean
  changingError: boolean
  changed: boolean
}
export type ForgotPasswordReducerType = Reducer<ForgotPasswordStateType>

export default rootReducer
