import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
  resizer: {
    maxWidth: 1000,
    width: '100%',
    alignSelf: 'center',
    height: '100%',
    // flexDirection: 'column',
  },
})

class Resizer extends Component<any> {
  render() {
    return (
      <View style={[styles.resizer, this.props.style]}>
        {this.props.children}
      </View>
    )
  }
}

export default Resizer
