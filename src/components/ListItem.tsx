import React from 'react'
import { Image, Platform, StyleSheet, View } from 'react-native'
import { Text, TouchableRipple } from 'react-native-paper'
import Media from './Media'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import { Hoverable } from 'react-native-web-hover'
// @ts-ignore
import { TouchableOpacity } from 'react-native-web'
import { getFriendlyDate } from '../dayjs'
import dayjs from 'dayjs'

const styles = StyleSheet.create({
  root: {
    borderRadius: 10,
    overflow: 'hidden',
  },
  rootInner: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 16,
    paddingBottom: 16,
    // borderBottomWidth: 1,
    // borderBottomColor: '#EDEDED',
  },

  middle: { flex: 1, paddingLeft: 16 },
  right: { paddingRight: 16, flexDirection: 'row' },

  brand: { fontSize: 13, fontWeight: '400' },
  name: { fontSize: 16, fontWeight: '600', paddingTop: 6, paddingBottom: 6 },
  sku: { opacity: 0.5, fontSize: 13, fontWeight: '400' },
  imageContainer: {
    height: 65,
    width: 90,
    // backgroundColor: '#EDEDED',
    borderRadius: 10,
    overflow: 'hidden',
    marginLeft: 12,
  },
  image: {
    height: 65,
    width: 100,
    borderRadius: 10,
  },
  total: {
    minWidth: 80,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 12,
  },
  totalText: {
    fontSize: 16,
  },
})

function ListItem({
  onPress,
  showImage,
  testID,
  safe,
  disabled,
  item,
  componentId,
  setSelectedItem,
  supplier,
}: {
  onPress?: any
  showImage?: boolean
  testID?: string
  safe: any
  disabled?: boolean
  item: any
  componentId: string
  setSelectedItem: (item: any) => any
  supplier: any
}) {
  // const dispatch = useDispatch()
  // const homeState = useSelector((state: any) => state.home)
  const edit = () => {
    setSelectedItem(item)
    console.log('wip')
  }
  return supplier?.first_name ? (
    <Hoverable key={supplier?.first_name}>
      {({ hovered }) => (
        <>
          {Platform.OS === 'web' ? (
            <TouchableOpacity
              onPress={edit}
              style={[
                styles.root,
                styles.rootInner,
                { paddingLeft: safe.left, paddingRight: safe.right },
                hovered
                  ? {
                      backgroundColor: '#E8E7EA',
                    }
                  : { backgroundColor: '#FFF' },
              ]}
            >
              {showImage && (
                <View style={styles.imageContainer}>
                  <Media
                    media={null}
                    mediaWidth={200}
                    renderPlaceholder={true}
                    render={(source: any) => (
                      <Image
                        source={source}
                        style={styles.image}
                        resizeMode="contain"
                      />
                    )}
                  />
                </View>
              )}
              <View style={styles.middle}>
                <>
                  <Text style={[styles.brand]}>
                    {supplier && supplier.first_name
                      ? supplier.first_name + ' ' + supplier.last_name
                      : supplier}
                  </Text>
                  <Text style={[styles.name]}>
                    {item?.numberOfPallets
                      ? `${item.numberOfPallets} ${
                          item.numberOfPallets === 1
                            ? 'europallet'
                            : 'europallets'
                        }`
                      : 'Unknown amount of europallets'}
                  </Text>
                  <Text style={[styles.sku]}>
                    {getFriendlyDate(
                      dayjs(new Date(item?.date?.seconds * 1000)),
                      false,
                      true
                    )}
                  </Text>
                </>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableRipple
              borderless={true}
              // useForeground={true}
              onPress={!disabled && onPress}
              accessibilityLabel={`hi native`}
              // accessibilityTraits={'button'}
              // accessibilityComponentType="button"
              accessibilityRole="button"
              style={styles.root}
              testID={testID}
            >
              <View
                style={[
                  styles.rootInner,
                  { paddingLeft: safe.left, paddingRight: safe.right },
                  hovered
                    ? { backgroundColor: '#E8E7EA' }
                    : { backgroundColor: '#efefef' },
                ]}
              >
                {showImage && (
                  <View style={styles.imageContainer}>
                    <Media
                      media={null}
                      mediaWidth={200}
                      renderPlaceholder={true}
                      render={(source: any) => (
                        <Image
                          source={source}
                          style={styles.image}
                          resizeMode="contain"
                        />
                      )}
                    />
                  </View>
                )}
                <View style={styles.middle}>
                  <>
                    <Text style={[styles.brand]}>brand</Text>
                    <Text style={[styles.name]}>title</Text>
                    <Text style={[styles.sku]}>date?</Text>
                  </>
                </View>
                <View style={styles.right}>
                  {/*<FAB*/}
                  {/*  onPress={() => alert('wip')}*/}
                  {/*  icon={'send'}*/}
                  {/*  small*/}
                  {/*  style={{ margin: 5 }}*/}
                  {/*/>*/}
                </View>
              </View>
            </TouchableRipple>
          )}
        </>
      )}
    </Hoverable>
  ) : null
}

export default safeAreaHOC(ListItem)
