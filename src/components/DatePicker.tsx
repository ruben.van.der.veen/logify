import React, { useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import { Button, TextInput, Title } from 'react-native-paper'
import { DatePickerModal, TimePickerModal } from 'react-native-paper-dates'

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    flexDirection: 'column',
  },
  button: {
    maxWidth: 250,
    marginBottom: 10,
  },
})

export function isValidDate(d: any, t?: string) {
  if (t && !/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(t)) {
    console.log('T succeeds')
  } else {
    console.log('T fails')
  }

  return ((d instanceof Date) as any) && !isNaN(d)
}

export function datePartsAreOK(parts: string[]) {
  // We receive parts as DD, MM, YYYY
  const day = Number(parts[0])
  const month = Number(parts[1])
  const year = Number(parts[2])
  if (isNaN(day) || day > 31) {
    console.log('day is incorrect')
    return false
  }
  if (isNaN(month) || month > 12) {
    console.log('month is incorrect')
    // MM is incorrect because month cannot be bigger than 12 over-all.
    return false
  }
  // is YYYY is not a number, the year is incorrect and we return false but if it is a valid number we return true after all the above statements ^
  return !isNaN(year)
}

type DateTimePickerPropsType = {
  mode: 'time' | 'date'
  onChange: (value: any) => any
  error?: any
  date: Date | undefined
  setError?: any
  disabled?: boolean
}
const hoursMap: { [key: number]: string } = {
  0: 'twelve',
  1: 'one',
  2: 'two',
  3: 'three',
  4: 'four',
  5: 'five',
  6: 'six',
  7: 'seven',
  8: 'eight',
  9: 'nine',
  10: 'ten',
  11: 'eleven',
  12: 'twelve',
  13: 'one',
  14: 'two',
  15: 'three',
  16: 'four',
  17: 'five',
  18: 'six',
  19: 'seven',
  20: 'eight',
  21: 'nine',
  22: 'ten',
  23: 'eleven',
}

function DateTimePicker({
  mode,
  onChange,
  date = new Date(),
  error,
  setError,
  disabled,
}: DateTimePickerPropsType) {
  const [visible, setVisible] = React.useState(false)
  const [timeVisible, setTimeVisible] = React.useState(false)
  const onDismiss = React.useCallback(() => {
    setVisible(false)
  }, [setVisible])
  const onDismissTime = React.useCallback(() => {
    setTimeVisible(false)
  }, [setTimeVisible])
  const getDateString = React.useCallback((date: Date | any) => {
    let dateString = new Intl.DateTimeFormat('nl', {
      month: '2-digit',
      day: '2-digit',
      year: 'numeric',
    }).format(date)
    // let mo = new Intl.DateTimeFormat('nl', { month: 'short' }).format(date)
    // let da = new Intl.DateTimeFormat('nl', { day: '2-digit' }).format(date)
    return `${dateString}`
  }, [])
  const getTimeString = React.useCallback((date: Date) => {
    let hours = ('0' + date.getHours()).substr(-2)
    let minutes = ('0' + date.getMinutes()).substr(-2)

    return `${hours}:${minutes}`
  }, [])

  const [val, setVal] = React.useState<string>(getDateString(date))

  useEffect(() => {
    if (date) {
      setVal(getDateString(date))
    }
  }, [date, getDateString, setVal])
  const onChangeFunc = React.useCallback(
    (result) => {
      setVisible(false)
      onChange({ date: result.date })
      setVal(getDateString(result.date))
    },
    [onChange, setVisible, setVal, getDateString]
  )

  const onConfirm = React.useCallback(
    ({ hours, minutes }) => {
      setTimeVisible(false)
      onChange({ hours, minutes })
    },
    [setTimeVisible, onChange]
  )

  const innerChange = (e: any) => {
    // const newDate = new Date(text)
    const text = e.target.value
    let dateVal
    if (text.includes('-')) {
      const dateParts = text.split('-')
      if (datePartsAreOK(dateParts)) {
        // This is because js date expects MM-DD-YYYY and dutch standard is DD-MM-YYYY
        dateVal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0])
      } else {
        setError('date', 'Date is invalid')
        return
      }
    } else if (text.includes('/')) {
      const dateParts = text.split('/')
      if (datePartsAreOK(dateParts)) {
        // This is because js date expects MM-DD-YYYY and dutch standard is DD-MM-YYYY
        dateVal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0])
      } else {
        setError('date', 'Date is invalid')
        return
      }
    }
    if (isValidDate(dateVal)) {
      onChange({ date: dateVal })
    } else {
      setVal(getDateString(date))
    }
  }
  return mode === 'date' ? (
    <>
      <DatePickerModal
        mode={'single'}
        visible={visible}
        onDismiss={onDismiss}
        date={date}
        onConfirm={onChangeFunc}
        saveLabel={'Save'} // optional
        label={'Select date'} // optional
        validRange={{
          startDate: new Date(),
        }}
      />
      <View style={{ flexDirection: 'row' }}>
        <TextInput
          mode={'outlined'}
          testID={'datepicker'}
          label={'Date'}
          style={{ width: '100%' }}
          onBlur={innerChange}
          onChangeText={(value: string) => setVal(value)}
          error={error}
          disabled={disabled}
          // style={styles.button}
          value={val}
          right={
            <TextInput.Icon
              name={'calendar'}
              onPress={() => setVisible(true)}
            />
          }
        />
      </View>
    </>
  ) : (
    <>
      <TimePickerModal
        visible={timeVisible}
        onDismiss={onDismissTime}
        onConfirm={onConfirm}
        hours={date.getHours()}
        minutes={date.getMinutes()}
        cancelLabel="Annuleren" // optional, default: 'Cancel'
        label={'Selecteer tijd'} // optional
      />
      <View style={{ flexDirection: 'row' }}>
        <Title
          style={{
            marginTop: 8,
            marginBottom: 6,
            marginLeft: 6,
            marginRight: 35,
            fontSize: 16,
          }}
        >
          Tijd:
        </Title>
        <Button
          onPress={() => setTimeVisible(true)}
          mode={'outlined'}
          style={styles.button}
          testID={'timepicker'}
          disabled={disabled}
          icon={
            'clock-time-' +
            hoursMap[date.getHours() + (date.getMinutes() > 30 ? 1 : 0)] +
            '-outline'
          }
        >
          {getTimeString(date)}
        </Button>
      </View>
    </>
  )
}

export default DateTimePicker
