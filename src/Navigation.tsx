import { Navigation } from 'react-native-navigation'
import * as routes from './Routes.config'

export function pop(componentId: string) {
  Navigation.pop(componentId)
}

export function push(componentId: string, pushOptions: any) {
  if (!pushOptions.component.options) {
    pushOptions.component.options = {}
  }
  if (!pushOptions.component.options.topBar) {
    pushOptions.component.options.topBar = {
      visible: false,
      animate: true,
      drawBehind: true,
    }
  }
  if (!pushOptions.component.options.statusBar) {
    pushOptions.component.options.statusBar = statusBar
  }

  if (!pushOptions.component.options.sideMenu) {
    pushOptions.component.options.sideMenu = {
      animationType: 'parallax',

      left: {
        visible: false,
        enabled: false,
      },
    }
  }

  Navigation.push(componentId, pushOptions)
}

export function pushHome(ignore: any) {
  // alert('GO HOME')
  // switchMenu(routes.HOME_ROUTE)
}

export function pushAuth() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: routes.AUTH_SCREEN_ROUTE,
            },
          },
        ],
        options: {
          topBar: {
            visible: false,
            animate: false,
            drawBehind: true,
          },
          statusBar,
        },
      },
    },
  })
}

type statusBarType = {
  backgroundColor: string
  style: 'dark' | 'light' | undefined
  drawBehind: boolean
}
const statusBar: statusBarType = {
  backgroundColor: '#fff',
  style: 'dark',
  drawBehind: true,
}

export function mergeOptions(componentId: string, options = {}) {
  Navigation.mergeOptions(componentId, options)
}

type TabObjectProps = {
  name: string
  text: string
  icon: string
  testID: string
}
export const getTabObject = ({ name, text, icon, testID }: TabObjectProps) => ({
  stack: {
    children: [
      {
        component: {
          name,
          options: {
            statusBar,
          },
        },
      },
    ],
    options: {
      layout: {
        backgroundColor: '#ffffff',
      },
      topBar: {
        visible: false,
        animate: false,
        drawBehind: true,
      },

      bottomTab: {
        text,
        icon,
        testID,
        selectedIconColor: '#0018A7',
        selectedTextColor: '#0018A7',
      },
    },
  },
})

export const getBottomTabs: () => any = () => ({
  children: [
    getTabObject({
      name: routes.HOME_SCREEN_ROUTE,
      text: 'Home',
      icon: require('./vectors/home/home-outline.svg'),
      testID: 'HOME',
    }),
    // getTabObject({
    //   name: routes.ACCOUNT_SCREEN_ROUTE,
    //   text: 'Account',
    //   icon: require('./vectors/user/icon-20.png'),
    //   testID: 'ACCOUNT',
    // }),
  ],
})

export function setHome(props?: any) {
  Navigation.setRoot({
    root: {
      bottomTabs: getBottomTabs(),
    },
  })
}

export default Navigation
