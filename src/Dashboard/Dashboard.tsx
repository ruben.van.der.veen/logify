import React, { useCallback, useEffect } from 'react'
import Resizer from '../components/Resizer'
import { FlatList, ScrollView, StyleSheet, View } from 'react-native'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import ListItem from '../components/ListItem'
import { useSelector } from 'react-redux'
import { isLoaded, useFirestore } from 'react-redux-firebase'
import { Button, Divider, Modal, Text, Title } from 'react-native-paper'
import { getFriendlyDate } from '../dayjs'
import dayjs from 'dayjs'
import mapboxgl from 'mapbox-gl' // or "const mapboxgl = require('mapbox-gl');"
import smallPallet from '../vectors/pallet.png'
import mediumPallet from '../vectors/palletMedium.png'
import largePallet from '../vectors/palletLarge.png'
import * as geofirestore from 'geofirestore'
import firebase from 'firebase/compat/app'

const styles = StyleSheet.create({
  footer: {
    height: 200,
  },
  brand: { fontSize: 13, fontWeight: '400' },
  title: { fontSize: 13, opacity: 0.5 },
  value: { fontSize: 14, fontWeight: '400', marginBottom: 6 },
  name: { fontSize: 16, fontWeight: '600', paddingTop: 6, paddingBottom: 6 },
  sku: { opacity: 0.5, fontSize: 13, fontWeight: '400' },
  titleStyle: {},
  map: {
    flex: 1,
    minHeight: 250,
    borderRadius: 10,
  },
})

export function categorizeJobs(allJobs: any[]) {
  let smallJobs: any[] = [],
    mediumJobs: any[] = [],
    largeJobs: any[] = []
  const item = (allJobs || []).map((job: any) => {
    if (job.numberOfPallets < 2) {
      smallJobs = [...smallJobs, job]
    } else if (job.numberOfPallets < 6) {
      mediumJobs = [...mediumJobs, job]
    } else {
      largeJobs = [...largeJobs, job]
    }
    return null
  })
  console.log(item)
  return { smallJobs, mediumJobs, largeJobs }
}

function Dashboard() {
  const [map, setMap] = React.useState(null)
  const mapContainer = React.useRef<any>(null)
  const firestore = useFirestore()
  const { allJobs } = useSelector((state: any) => state.firestore.ordered)
  const GeoFirestore = geofirestore.initializeApp(firestore)
  const geoCollection = GeoFirestore.collection('jobs')

  const { uid } = useSelector((state: any) => state.firebase.auth)

  const zoomChanged = useCallback(
    (center: { lat: number; lng: number }, zoom: number) => {
      const geoQuery = geoCollection.near({
        center: new firebase.firestore.GeoPoint(center.lat, center.lng),
        radius: 25,
      })
      console.log(geoCollection)
      geoQuery.get().then((value) => {
        // All GeoDocument returned by GeoQuery, like the GeoDocument added above
        console.log(value.docs)
      })
    },
    [geoCollection]
  )

  useEffect(() => {
    mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_KEY || ''
    const initializeMap = ({
      setMap,
      mapContainer,
    }: {
      setMap: (map: any) => any
      mapContainer: any
    }) => {
      const map = new mapboxgl.Map({
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [5.5, 52.17],
        zoom: 5.8,
      })
      map.on('zoom', () => zoomChanged(map.getCenter(), map.getZoom()))
      map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true,
          },
          // When active the map will receive updates to the device's location as it changes.
          trackUserLocation: true,
          // Draw an arrow next to the location dot to indicate which direction the device is heading.
          showUserHeading: true,
        })
      )

      map.on('load', () => {
        const { smallJobs, mediumJobs, largeJobs } = categorizeJobs(allJobs)
        console.log({ smallJobs, mediumJobs, largeJobs })
        if (smallJobs.length > 0) {
          map.loadImage(smallPallet, (error, image: any) => {
            if (error) throw error
            map.addImage('small-marker', image, { pixelRatio: 12 })
            const jobFeatures = smallJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  title: job.numberOfPallets + ' europallet',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('smallpoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'smallpoints',
              type: 'symbol',
              source: 'smallpoints',
              layout: {
                'icon-image': 'small-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }
        if (mediumJobs.length > 0) {
          map.loadImage(mediumPallet, (error, image: any) => {
            if (error) throw error
            map.addImage('medium-marker', image, { pixelRatio: 12 })
            const jobFeatures = mediumJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  title: job.numberOfPallets + ' europallets',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('mediumpoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'mediumpoints',
              type: 'symbol',
              source: 'mediumpoints',
              layout: {
                'icon-image': 'medium-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }
        if (largeJobs.length > 0) {
          map.loadImage(largePallet, (error, image: any) => {
            if (error) throw error
            map.addImage('large-marker', image, { pixelRatio: 12 })
            const jobFeatures = largeJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  description:
                    '<strong>Make it Mount Pleasant</strong><p><a href="http://www.mtpleasantdc.com/makeitmtpleasant" target="_blank" title="Opens in a new window">Make it Mount Pleasant</a> is a handmade and vintage market and afternoon of live entertainment and kids activities. 12:00-6:00 p.m.</p>',
                  title: job.numberOfPallets + ' europallets',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('largepoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'largepoints',
              type: 'symbol',
              source: 'largepoints',
              layout: {
                'icon-image': 'large-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }

        setMap(map)
        map.resize()
      })
    }

    if (!map && isLoaded(allJobs)) initializeMap({ setMap, mapContainer })
  }, [map, allJobs, zoomChanged])

  const [selectedItem, setSelectedItem] = React.useState<undefined | any>()

  const takeJob = () => {
    console.log(selectedItem)
    firestore.collection('jobs').doc(selectedItem.id).update({
      carrier: uid,
      status: 'IN_PROGRESS',
    })
    setSelectedItem(undefined)
  }

  const renderItem = ({ item }: { item: any }) => {
    return (
      <ListItem
        item={item}
        showImage={true}
        setSelectedItem={setSelectedItem}
      />
    )
  }

  return [
    <ScrollView style={{ margin: 16, height: '100%' }}>
      <Resizer>
        <>
          <View style={styles.titleStyle}>
            <Title>Available jobs</Title>
          </View>
          <View ref={(el) => (mapContainer.current = el)} style={styles.map} />
          <FlatList
            key={`list`}
            keyboardShouldPersistTaps={'handled'}
            keyboardDismissMode={'on-drag'}
            renderItem={renderItem}
            data={allJobs}
            numColumns={1}
            style={{ flex: 1 }}
            contentInsetAdjustmentBehavior={'never'}
          />
        </>
      </Resizer>
    </ScrollView>,
    <Modal
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      contentContainerStyle={{
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
      }}
      visible={!!selectedItem}
      onDismiss={() => setSelectedItem(undefined)}
    >
      <Title>Job summary</Title>
      <Divider />
      <>
        <Text style={styles.title}>Supplier</Text>
        <Text style={styles.value}>{selectedItem?.supplier}</Text>
        <Text style={styles.title}>Number of pallets</Text>
        <Text style={styles.value}>
          {selectedItem?.numberOfPallets
            ? `${selectedItem.numberOfPallets} ${
                selectedItem.numberOfPallets === 1
                  ? 'europallet'
                  : 'europallets'
              }`
            : 'Unknown amount of europallets'}
        </Text>
        <Text style={styles.title}>Date</Text>
        <Text style={styles.value}>
          {getFriendlyDate(
            dayjs(new Date(selectedItem?.date?.seconds * 1000)),
            false,
            true
          )}
        </Text>
      </>
      <br />
      <Title>Do you want to take this job?</Title>
      <View style={{ flexDirection: 'row', flexGrow: 1, marginTop: 50 }}>
        <Button
          onPress={takeJob}
          mode={'contained'}
          style={{ flex: 1, marginRight: 5 }}
        >
          <Text style={{ color: '#FFF' }}>Yes</Text>
        </Button>
        <Button
          onPress={() => setSelectedItem(undefined)}
          mode={'contained'}
          color={'#EFEFEF'}
          style={{ flex: 1, marginLeft: 5 }}
        >
          <Text style={{ color: '#000' }}>No</Text>
        </Button>
      </View>
    </Modal>,
  ]
}

export default safeAreaHOC(Dashboard)
