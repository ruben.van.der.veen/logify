import actions from './actions'
import saga from '../GeneralListFunctions/sagaFactory'

const sagaConst = () =>
  saga(
    actions,
    () => 'v1/dashboard',
    (state) => state.dashboard
  )
export default sagaConst
