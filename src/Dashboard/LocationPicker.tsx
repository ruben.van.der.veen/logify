import React, { useEffect } from 'react'
import { StyleSheet, View } from 'react-native'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import mapboxgl from 'mapbox-gl'
import { Title } from 'react-native-paper'

// type HomeProps = {
//   componentId: string
//   safe: any
// }

const styles = StyleSheet.create({
  map: {
    flex: 1,
    minHeight: 200,
    maxHeight: 200,
    borderRadius: 10,
  },
})

function LocationPicker({ onMapPress }: { onMapPress: (coords: any) => any }) {
  const [map, setMap] = React.useState(null)
  const mapContainer = React.useRef<any>(null)

  useEffect(() => {
    mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_KEY || ''
    const initializeMap = ({
      setMap,
      mapContainer,
    }: {
      setMap: (map: any) => any
      mapContainer: any
    }) => {
      const map = new mapboxgl.Map({
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [5.5, 52.17],
        zoom: 5.8,
      })
      map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true,
          },
          // When active the map will receive updates to the device's location as it changes.
          trackUserLocation: true,
          // Draw an arrow next to the location dot to indicate which direction the device is heading.
          showUserHeading: true,
        })
      )
      const marker = new mapboxgl.Marker()

      map.on('click', (props: any) => {
        const coordinates = props.lngLat
        onMapPress(coordinates)

        marker.setLngLat(coordinates).addTo(map)
      })
      map.on('load', () => {
        setMap(map)
        map.resize()
      })
    }

    if (!map) initializeMap({ setMap, mapContainer })
  }, [map, onMapPress])

  return (
    <>
      <Title style={{ fontSize: 16 }}>Location</Title>
      <View ref={(el) => (mapContainer.current = el)} style={styles.map} />
    </>
  )
}

export default safeAreaHOC(LocationPicker)
