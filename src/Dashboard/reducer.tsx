import actions from './actions'
import reducer from '../GeneralListFunctions/reducerFactory'

export default reducer(actions)
