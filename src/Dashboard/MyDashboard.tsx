import React, { useCallback, useEffect } from 'react'
import Resizer from '../components/Resizer'
import { FlatList, ScrollView, StyleSheet, View } from 'react-native'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import ListItem from '../components/ListItem'
import { useSelector } from 'react-redux'
import { isLoaded, useFirestore } from 'react-redux-firebase'
import {
  Button,
  Divider,
  FAB,
  IconButton,
  Modal,
  Text,
  TextInput,
  Title,
  useTheme,
} from 'react-native-paper'
import * as geofirestore from 'geofirestore'
import firebase from 'firebase/compat'
import mapboxgl from 'mapbox-gl'
import smallPallet from '../vectors/pallet.png'
import mediumPallet from '../vectors/palletMedium.png'
import largePallet from '../vectors/palletLarge.png'
import { categorizeJobs } from './Dashboard'
import { colors } from '../Auth/styles'
import DatePicker from '../components/DatePicker'
import LocationPicker from './LocationPicker'

// type HomeProps = {
//   componentId: string
//   safe: any
// }

const styles = StyleSheet.create({
  footer: {
    height: 200,
  },
  input: {
    marginBottom: 16,
    marginTop: 12,
  },
  brand: { fontSize: 13, fontWeight: '400' },
  name: { fontSize: 16, fontWeight: '600', paddingTop: 6, paddingBottom: 6 },
  sku: { opacity: 0.5, fontSize: 13, fontWeight: '400' },
  titleStyle: {},
  map: {
    flex: 1,
    minHeight: 250,
    borderRadius: 10,
  },
})

function MyDashboard() {
  const [selectedItem, setSelectedItem] = React.useState<undefined | any>()
  const [creating, setCreating] = React.useState<boolean>(false)
  const firestore = useFirestore()

  const { profile } = useSelector((state: any) => state.firestore.ordered)

  const role = profile && profile[0] && profile[0].role
  const isSupplier = role === 'supplier'
  const isTransporter = role === 'transporter'
  const { uid } = useSelector((state: any) => state.firebase.auth)
  const [map, setMap] = React.useState(null)
  const mapContainer = React.useRef<any>(null)
  const GeoFirestore = geofirestore.initializeApp(firestore)
  const geoCollection = GeoFirestore.collection('jobs')
  const { myJobs, mySupplierJobs } = useSelector(
    (state: any) => state.firestore.ordered
  )

  const zoomChanged = useCallback(
    (center: { lat: number; lng: number }, zoom: number) => {
      const geoQuery = geoCollection.near({
        center: new firebase.firestore.GeoPoint(center.lat, center.lng),
        radius: 25,
      })
      console.log(geoCollection)
      geoQuery.get().then((value) => {
        // All GeoDocument returned by GeoQuery, like the GeoDocument added above
        console.log(value.docs)
      })
    },
    [geoCollection]
  )

  useEffect(() => {
    mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_KEY || ''
    const initializeMap = ({
      setMap,
      mapContainer,
    }: {
      setMap: (map: any) => any
      mapContainer: any
    }) => {
      const map = new mapboxgl.Map({
        container: mapContainer.current,
        style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
        center: [5.5, 52.17],
        zoom: 5.8,
      })
      map.on('zoom', () => zoomChanged(map.getCenter(), map.getZoom()))
      map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true,
          },
          // When active the map will receive updates to the device's location as it changes.
          trackUserLocation: true,
          // Draw an arrow next to the location dot to indicate which direction the device is heading.
          showUserHeading: true,
        })
      )

      map.on('load', () => {
        console.log(mySupplierJobs)
        const { smallJobs, mediumJobs, largeJobs } = categorizeJobs(
          isTransporter ? myJobs : mySupplierJobs
        )
        console.log({ smallJobs, mediumJobs, largeJobs })
        if (smallJobs.length > 0) {
          map.loadImage(smallPallet, (error, image: any) => {
            if (error) throw error
            map.addImage('small-marker', image, { pixelRatio: 12 })
            const jobFeatures = smallJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  title: job.numberOfPallets + ' europallet',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('smallpoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'smallpoints',
              type: 'symbol',
              source: 'smallpoints',
              layout: {
                'icon-image': 'small-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }
        if (mediumJobs.length > 0) {
          map.loadImage(mediumPallet, (error, image: any) => {
            if (error) throw error
            map.addImage('medium-marker', image, { pixelRatio: 12 })
            const jobFeatures = mediumJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  title: job.numberOfPallets + ' europallets',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('mediumpoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'mediumpoints',
              type: 'symbol',
              source: 'mediumpoints',
              layout: {
                'icon-image': 'medium-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }
        if (largeJobs.length > 0) {
          map.loadImage(largePallet, (error, image: any) => {
            if (error) throw error
            map.addImage('large-marker', image, { pixelRatio: 12 })
            const jobFeatures = largeJobs.map((job: any) => {
              return {
                type: 'Feature',
                geometry: {
                  type: 'Point',
                  coordinates: [job.coordinates._long, job.coordinates._lat],
                },
                properties: {
                  description:
                    '<strong>Make it Mount Pleasant</strong><p><a href="http://www.mtpleasantdc.com/makeitmtpleasant" target="_blank" title="Opens in a new window">Make it Mount Pleasant</a> is a handmade and vintage market and afternoon of live entertainment and kids activities. 12:00-6:00 p.m.</p>',
                  title: job.numberOfPallets + ' europallets',
                },
              }
            })
            // Add a GeoJSON source
            map.addSource('largepoints', {
              type: 'geojson',
              data: {
                type: 'FeatureCollection',
                features: jobFeatures as any,
              },
            })

            // Add a symbol layer
            map.addLayer({
              id: 'largepoints',
              type: 'symbol',
              source: 'largepoints',
              layout: {
                'icon-image': 'large-marker',
                'icon-allow-overlap': true,
                // get the title name from the source's "title" property
                'text-field': ['get', 'title'],
                'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                'text-offset': [0, 1.25],
                'text-anchor': 'top',
              },
            })
          })
        }

        setMap(map)
        map.resize()
      })
    }

    if (!map && isLoaded(myJobs)) initializeMap({ setMap, mapContainer })
  }, [map, myJobs, zoomChanged, isTransporter, mySupplierJobs])
  const cancelJob = () => {
    firestore.collection('jobs').doc(selectedItem.id).update({
      carrier: uid,
      status: 'OPEN',
    })
    setSelectedItem(undefined)
  }
  const theme = useTheme()
  const renderItem = ({ item }: { item: any }) => {
    let supplier = item?.supplier

    firestore
      .collection('users')
      .doc(item?.supplier)
      .get()
      .then((it) => {
        console.log(it.data())
        supplier = it.data()
        return (
          <ListItem
            supplier={supplier}
            item={item}
            showImage={true}
            setSelectedItem={setSelectedItem}
          />
        )
      })
    return null
  }
  const createJob = () => {
    setCreating(true)
  }
  const [booking, setBooking] = React.useState<{
    numberOfPallets?: any
    date?: Date
    coordinates?: { latitude: number; longitude: number }
    supplier: string
    status: string
  }>({ date: new Date(), supplier: uid, status: 'OPEN' })

  const onChangeBooking = useCallback(
    (key: string, value: any) => {
      let newBooking = booking
      // @ts-ignore
      newBooking[key] = value
      setBooking(newBooking)
    },
    [booking, setBooking]
  )

  const controlMapPress = useCallback(
    (props: { lat: number; lng: number }) => {
      let newBooking = booking
      newBooking.coordinates = new firebase.firestore.GeoPoint(
        props.lat,
        props.lng
      )
      setBooking(newBooking)
    },
    [booking, setBooking]
  )

  const saveBooking = useCallback(() => {
    geofirestore.initializeApp(firestore)
    const geoCollection = GeoFirestore.collection('jobs')
    let cleanBooking = booking
    if (cleanBooking.numberOfPallets) {
      cleanBooking.numberOfPallets = Number(cleanBooking.numberOfPallets)
    }
    geoCollection.add(booking).then(() => {
      setBooking({ date: new Date(), supplier: uid, status: 'OPEN' })
      setCreating(false)
    })
  }, [booking, setBooking, GeoFirestore, firestore, uid])

  return [
    <ScrollView style={{ margin: 16, height: '100%' }} key={'scrollview'}>
      <Resizer>
        <>
          <View style={styles.titleStyle}>
            <Title>{isTransporter ? 'My jobs' : 'My bookings'}</Title>
          </View>
          <View ref={(el) => (mapContainer.current = el)} style={styles.map} />
          <FlatList
            key={`list`}
            keyboardShouldPersistTaps={'handled'}
            keyboardDismissMode={'on-drag'}
            renderItem={renderItem}
            data={isSupplier ? mySupplierJobs : myJobs}
            numColumns={1}
            style={{ flex: 1 }}
            contentInsetAdjustmentBehavior={'never'}
          />
        </>
      </Resizer>
    </ScrollView>,
    isSupplier && (
      <FAB
        key={'addFab'}
        style={{
          position: 'absolute',
          bottom: 16,
          right: 16,
          backgroundColor: theme.colors.primary,
        }}
        icon={'plus'}
        onPress={createJob}
        theme={theme}
        color={'#FFF'}
      />
    ),
    <Modal
      key={'addmodal'}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      contentContainerStyle={{
        backgroundColor: 'white',
        padding: 20,
        width: '100%',
        height: '100%',
      }}
      visible={creating}
      onDismiss={() => setCreating(false)}
      dismissable
    >
      <View style={{ flexDirection: 'row', marginBottom: 8 }}>
        <Title style={{ flex: 1 }}>New booking</Title>
        <IconButton
          icon={'close'}
          style={{ margin: 0 }}
          onPress={() => setCreating(false)}
          color={'#000'}
        />
      </View>
      <Divider />

      <View style={{ flexDirection: 'column', flexGrow: 1, marginTop: 8 }}>
        <LocationPicker onMapPress={controlMapPress} />
        <TextInput
          mode="outlined"
          theme={{ colors: { primary: colors.secondary } }}
          style={styles.input}
          label={'Number of (euro)pallets'}
          keyboardType={'numeric'}
          onChangeText={(value) => onChangeBooking('numberOfPallets', value)}
          value={booking?.numberOfPallets}
        />

        <DatePicker
          mode={'date'}
          onChange={(value: any) => onChangeBooking('date', value)}
          date={new Date()}
        />
        <Button
          onPress={saveBooking}
          mode={'contained'}
          style={{ marginTop: 12 }}
        >
          <Text style={{ color: '#FFF' }}>Save</Text>
        </Button>
      </View>
    </Modal>,
    <Modal
      key={'cancelmodal'}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      contentContainerStyle={{
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
      }}
      visible={!!selectedItem}
      onDismiss={() => setSelectedItem(undefined)}
    >
      <Title>Are you sure you want to cancel this job?</Title>
      <View style={{ flexDirection: 'row', flexGrow: 1, marginTop: 50 }}>
        <Button
          onPress={cancelJob}
          mode={'contained'}
          style={{ flex: 1, marginRight: 5 }}
        >
          <Text style={{ color: '#FFF' }}>Yes</Text>
        </Button>
        <Button
          onPress={() => setSelectedItem(undefined)}
          mode={'contained'}
          color={'#EFEFEF'}
          style={{ flex: 1, marginLeft: 5 }}
        >
          <Text style={{ color: '#000' }}>No</Text>
        </Button>
      </View>
    </Modal>,
  ]
}

export default safeAreaHOC(MyDashboard)
