import { applyMiddleware, compose, createStore } from 'redux'

import rootReducer from './reducers'
import mySaga from './sagas'
import firebase from 'firebase/compat/app'
import { createFirestoreInstance } from 'redux-firestore' // <- needed if using firestore
import createSagaMiddleware from 'redux-saga'

declare global {
  interface Window {
    store: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose

export default function configureStore() {
  // create the saga middleware
  const sagaMiddleware = createSagaMiddleware()

  // mount it on the Store
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware))
  )
  const rrfConfig = {
    userProfile: 'users',
    useFirestoreForProfile: true, // Firestore for Profile instead of Realtime DB
    // enableClaims: true // Get custom claims along with the profile
  }
  const rrfProps = {
    firebase,
    config: rrfConfig,
    dispatch: store.dispatch,
    createFirestoreInstance, // <- needed if using firestore
  }

  // debugging
  window.store = store

  // then run the saga
  sagaMiddleware.run(mySaga)

  return { store, rrfProps }
}

const initialState = {}

// const initialState = process.env.NODE_ENV === 'development' ? dev : {}
