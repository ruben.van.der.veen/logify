import React from 'react'
import Resizer from '../components/Resizer'
import {
  Image,
  ImageSourcePropType,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import { Title } from 'react-native-paper'
import Truck from '../vectors/truck.png'
import Money from '../vectors/money-bag.png'
import Globe from '../vectors/globe.png'
import Route from '../vectors/route.png'
import { push } from '../Navigation'
import { useSelector } from 'react-redux'

// type HomeProps = {
//   componentId: string
//   safe: any
// }

const styles = StyleSheet.create({
  footer: {
    height: 200,
  },
  bigButtonRoot: {
    flexDirection: 'column',
    borderRadius: 10,
    border: '3px solid #58B259',
    padding: 20,
    minWidth: 150,
    marginLeft: 16,
    marginTop: 16,
    minHeight: 150,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
  },
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginRight: 16,
  },
  titleStyle: {
    margin: 16,
    marginBottom: 60,
  },
  fontStyle: { fontSize: 32 },
})

function BigButton({
  img,
  title,
  componentId,
  link,
}: {
  img: ImageSourcePropType
  title: string
  componentId: string
  link: string
}) {
  const goTo = () => {
    push(componentId, {
      component: { name: link },
    })
  }
  return (
    <TouchableOpacity style={styles.bigButtonRoot} onPress={goTo}>
      <Image source={img} style={styles.image} resizeMode="contain" />
      <Title>{title}</Title>
    </TouchableOpacity>
  )
}

function Home({ componentId }: { componentId: string }) {
  const { profile } = useSelector((state: any) => state.firestore.ordered)
  const role = profile && profile[0].role
  const isSupplier = role === 'supplier'
  const isTransporter = role === 'transporter'
  return [
    <ScrollView>
      <Resizer>
        <View style={styles.titleStyle}>
          <Title style={styles.fontStyle}>Welcome {role}!</Title>
        </View>
        <View style={styles.container}>
          {isTransporter && (
            <BigButton
              img={Route as ImageSourcePropType}
              title={'Available jobs'}
              componentId={componentId}
              link={'/dashboard'}
            />
          )}
          {isTransporter && (
            <BigButton
              img={Truck as ImageSourcePropType}
              title={'Availability'}
              componentId={componentId}
              link={'/availability'}
            />
          )}
          <BigButton
            img={Globe as ImageSourcePropType}
            title={isSupplier ? 'My bookings' : 'My jobs'}
            componentId={componentId}
            link={'/my-jobs'}
          />
          <BigButton
            img={Money as ImageSourcePropType}
            title={'Invoices'}
            componentId={componentId}
            link={'/invoices'}
          />
        </View>
      </Resizer>
    </ScrollView>,
  ]
}

export default safeAreaHOC(Home)
