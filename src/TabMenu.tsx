import React from 'react'
import { connect, useSelector } from 'react-redux'
import { Image, ImageSourcePropType, StyleSheet, View } from 'react-native'
import { Link, withRouter } from 'react-router-dom'
import {
  Avatar,
  Badge,
  List,
  Title,
  TouchableRipple,
  withTheme,
} from 'react-native-paper'
// import Logo from './Logo'
import { getGravatar } from './helpers/gravatar'
import { push } from './Navigation'
import { Theme } from 'react-native-paper/lib/typescript/types'
import { AvatarImageSource } from 'react-native-paper/lib/typescript/components/Avatar/AvatarImage'
import { RootState } from './reducers'
import UserIcon from './vectors/user/icon-120.png'
import Logo from './vectors/logo-white.png'
import Truck from './vectors/truck.png'
import Money from './vectors/money-bag.png'
import Globe from './vectors/globe.png'
import Route from './vectors/route.png'
import { useFirestoreConnect } from 'react-redux-firebase'
import dayjs from 'dayjs'
import { switchMenu } from './Navigation.web'

const styles = StyleSheet.create({
  root: {
    position: 'relative',
    backgroundColor: '#fff',
    height: 70,
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 4.84,
    elevation: 5,
    zIndex: 1000,
    alignItems: 'center',
    flexWrap: 'wrap',
    // border: '1px solid black',
  },
  rootMedium: {
    paddingLeft: 16,
    paddingRight: 16,
    height: 70,
  },

  tabRoot: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    maxWidth: 250,
  },
  tabRootMedium: { flexDirection: 'row' },
  tabRootLarge: { flexDirection: 'row' },

  tab: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingTop: 10,
    width: '100%',
  },
  tabLarge: {
    flexDirection: 'row',
    paddingTop: 0,
  },

  tabLink: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  tabIcon: {
    width: 20,
    height: 20,
  },
  tabIconLarge: {
    width: 30,
    height: 30,
  },
  tabLabel: {
    marginTop: 6,
    fontSize: 12,
  },
  tabLabelLarge: {
    marginLeft: 12,
    fontSize: 14,
  },

  space: {
    flex: 1,
    flexDirection: 'row',
  },

  avatar: {
    marginLeft: 16,
    // backgroundColor: '#FFF',
    // padding: 20,
  },

  avatarContainer: {},

  search: {
    borderRadius: 60,
    elevation: 0,
    backgroundColor: '#f8f8f8',
    height: 46,
    fontSize: 16,
    flex: 1,
    minWidth: 350,
  },
  tabMedium: {
    // paddingLeft: 50,
    justifyContent: 'flex-start',
  },
  logo: { flexDirection: 'row', marginRight: 20 },
  logoImg: { width: 150, height: 60 },
})
type TabType = {
  label: string
  icon: ImageSourcePropType
  selected: boolean
  theme: Theme
  url: string
  isMedium: boolean
  isLarge: boolean
  componentId: string
  badge?: number | string
  badgeColor?: string
  isSupplier?: boolean
}

function Tab({
  label,
  icon,
  selected,
  theme,
  url,
  isMedium,
  isLarge,
  componentId,
  badge,
  badgeColor,
  isSupplier,
}: TabType) {
  return (
    <TouchableRipple
      onPress={() => {
        push(componentId, {
          component: { name: url },
        })
      }}
      style={[
        styles.tabRoot,
        isMedium && styles.tabRootMedium,
        isLarge && styles.tabRootLarge,
      ]}
      accessibilityLabel={`Go to ${label}`}
      // @ts-ignore
      accessibilityTraits={'button'}
      accessibilityComponentType="button"
      accessibilityRole="button"
    >
      <View
        style={[
          styles.tab,
          isMedium && styles.tabMedium,
          isLarge && styles.tabLarge,
        ]}
      >
        {badge && (
          <Badge
            style={[
              isLarge
                ? badgeColor
                  ? {
                      backgroundColor: '#FFF',
                      color: '#58B259',
                      position: 'absolute',
                      top: 0,
                      left: -15,
                      zIndex: 11,
                    }
                  : { position: 'absolute', top: 0, left: -15, zIndex: 11 }
                : isMedium
                ? badgeColor
                  ? {
                      backgroundColor: '#FFF',
                      color: '#58B259',
                      position: 'absolute',
                      top: 0,
                      left: 55,
                      zIndex: 11,
                    }
                  : {
                      position: 'absolute',
                      top: 0,
                      left: 55,
                      zIndex: 11,
                    }
                : badgeColor
                ? {
                    backgroundColor: '#FFF',
                    color: '#58B259',
                    position: 'absolute',
                    top: 5,
                    right: isSupplier ? 35 : 15,
                  }
                : {
                    position: 'absolute',
                    top: 5,
                    right: isSupplier ? 35 : 15,
                  },
            ]}
          >
            {badge}
          </Badge>
        )}
        <Image
          source={icon}
          style={[
            styles.tabIcon,
            isLarge && styles.tabIconLarge,
            { tintColor: '#FFF' },
          ]}
          resizeMode="center"
        />
        <Title
          style={[
            styles.tabLabel,
            isLarge && styles.tabLabelLarge,
            { color: '#FFF' },
          ]}
        >
          {label}
        </Title>
      </View>
    </TouchableRipple>
  )
}

const isSelectedStrict = (path: string, location: { pathname: string }) => {
  if (location && location.pathname) {
    return location.pathname === path
  }
  return false
}

const isSelected = (path: string, location: { pathname: string }) => {
  if (location && location.pathname) {
    return location.pathname.includes(path)
  }
  return false
}

export const locationIncludes = (location: { pathname: string }, v: string) => {
  return !!(location && location.pathname && location.pathname.includes(v))
}

function TabMenu({
  organization,
  user,
  theme,
  location,
  isLarge,
  isMedium,
  // searchValue,
  componentId,
}: any) {
  const auth = useSelector((state: any) => state.firebase.auth)
  console.log(auth)
  useFirestoreConnect([
    {
      collection: 'availability',
      doc: auth.uid,
    },
    {
      collection: 'jobs',
      where: ['status', '==', 'OPEN'],
      storeAs: 'allJobs',
    },
    {
      collection: 'jobs',
      where: ['status', '==', 'IN_PROGRESS', 'carrier', '==', auth.uid],
      storeAs: 'myJobs',
    },
    {
      collection: 'jobs',
      where: ['supplier', '==', auth.uid],
      storeAs: 'mySupplierJobs',
    },
    {
      collection: 'users',
      doc: auth.uid,
      storeAs: 'profile',
    },
  ])

  const { availability, allJobs, myJobs, profile } = useSelector(
    (state: any) => state.firestore.ordered
  )

  const role = profile && profile[0] && profile[0].role
  const isSupplier = role === 'supplier'
  const isTransporter = role === 'transporter'

  const list = ((availability && availability) || []).map((it: any) => {
    return {
      ...it,
      dates: it.dates.map((date: any) => {
        return new Date(date.seconds * 1000)
      }),
    }
  })
  let isAvailableToday = false
  if (
    list &&
    list.length > 0 &&
    list[0].dates &&
    list[0].dates.some((date: any) => dayjs(date).isToday())
  ) {
    isAvailableToday = true
  }
  if (locationIncludes(location, 'auth')) {
    return null
  }
  if (locationIncludes(location, 'change-password')) {
    return null
  }
  if (locationIncludes(location, 'request-account')) {
    return null
  }
  if (locationIncludes(location, 'forgot-password')) {
    return null
  }

  const tabProps = {
    isLarge,
    isMedium,
    theme,
  }
  return (
    <View
      style={[
        styles.root,
        isMedium && styles.rootMedium,
        {
          backgroundColor: theme.colors.primary,
        },
      ]}
    >
      {isLarge || isMedium ? (
        <Link to={'/'} style={{ textDecoration: 'none' }}>
          <View style={styles.logo}>
            <Image
              source={Logo as ImageSourcePropType}
              style={[styles.logoImg, { tintColor: '#FFF' }]}
              resizeMode="center"
            />{' '}
          </View>
        </Link>
      ) : null}

      <View
        style={[
          { flex: 1, flexDirection: 'row' },
          isLarge && { marginLeft: 48 },
        ]}
      >
        {isTransporter && (
          <Tab
            componentId={componentId}
            url={'/dashboard'}
            icon={Route as ImageSourcePropType}
            label={'Jobs'}
            selected={isSelectedStrict('/dashboard', location)}
            {...tabProps}
            badge={allJobs && allJobs.length > 0 ? allJobs.length : undefined}
            isSupplier={isSupplier}
          />
        )}
        {isTransporter && (
          <Tab
            componentId={componentId}
            url={'/availability'}
            icon={Truck as ImageSourcePropType}
            label={'Availability'}
            selected={isSelected('/availability', location)}
            {...tabProps}
            badge={isAvailableToday ? '✓' : undefined}
            badgeColor={isAvailableToday ? 'green' : undefined}
            isSupplier={isSupplier}
          />
        )}
        <Tab
          componentId={componentId}
          url={'/my-jobs'}
          icon={Globe as ImageSourcePropType}
          label={isSupplier ? 'My bookings' : 'My jobs'}
          selected={isSelected('/my-jobs', location)}
          {...tabProps}
          badge={myJobs && myJobs.length > 0 ? myJobs.length : undefined}
          isSupplier={isSupplier}
        />
        <Tab
          componentId={componentId}
          url={'/invoice'}
          icon={Money as ImageSourcePropType}
          label={'Invoices'}
          selected={isSelected('/invoice', location)}
          {...tabProps}
          isSupplier={isSupplier}
        />

        {!isMedium && !isLarge && auth ? (
          <Tab
            componentId={componentId}
            url={'/account'}
            icon={UserIcon as ImageSourcePropType}
            label={'Account'}
            selected={isSelected('/account', location)}
            isSupplier={isSupplier}
            {...tabProps}
          />
        ) : null}
      </View>
      {isLarge || isMedium ? (
        <List.Item
          left={() => (
            <>
              <Avatar.Image
                size={46}
                source={
                  getGravatar(
                    (auth && auth.email) || '',
                    35 * 3
                  ) as AvatarImageSource
                }
                style={styles.avatar}
              />
            </>
          )}
          // title={[user.first_name, user.preposition, user.last_name]
          //   .filter((n) => n)
          //   .join(' ')}
          titleStyle={{ color: '#FFF' }}
          descriptionStyle={{ color: '#EFEFEF' }}
          title={'Welcome'}
          // description={organization.name}
          description={(auth && auth.email) || 'Onbekende user'}
          onPress={() => switchMenu('/account')}
        />
      ) : null}
    </View>
  )
}

const mapStateToProps = (state: RootState, props: any) => ({
  // user: (state.auth && state.auth.user && state.auth.user.user) || {},
  // organization: state.auth && state.auth.user && state.auth.user.organization,
  // searchValue: state.search.searchValue.reducerValue,
})
export default withRouter(connect(mapStateToProps)(withTheme(TabMenu)))
