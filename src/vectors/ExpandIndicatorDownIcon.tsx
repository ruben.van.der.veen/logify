import React from 'react'

export const width = 6
export const height = 9

const Vector = (props: any) => (
  <svg fill={props.color} width={24} height={24} viewBox="0 0 24 20">
    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
    <path d="M0-.75h24v24H0z" fill="none" />
  </svg>
)

export default Vector
