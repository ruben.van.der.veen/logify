import { AppRegistry } from 'react-native'
import App from './App'
import './index.css'

import React from 'react'

function AppComp() {
  return <App />
}

AppRegistry.registerComponent('logify', () => AppComp)

AppRegistry.runApplication('logify', {
  rootTag: document.getElementById('root'),
})
