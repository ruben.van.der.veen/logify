import { AppRegistry } from 'react-native'
import App from './App'
import { name as appName } from '../app.json'
import React from 'react'
import Navigation, { setHome } from './Navigation'
import { registerScreens } from './Routes.native'

class AppComp extends React.Component {
  render() {
    return <App />
  }
}

AppRegistry.registerComponent(appName, () => AppComp)

registerScreens()

Navigation.events().registerAppLaunchedListener(() => {
  setHome()
})
