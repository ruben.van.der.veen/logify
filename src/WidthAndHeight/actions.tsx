import { actionCreator as ac } from '../helpers/actionCreator'

const ns = (type: string) => `WIDTH_AND_HEIGHT_${type}`

export const UPDATE = ns('UPDATE')
export const update = ac(UPDATE)
