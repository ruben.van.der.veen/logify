import React, { ReactChildren } from 'react'
import { Appbar } from 'react-native-paper'
import safeAreaHOC from './safeAreaHOC'
import { StyleProp } from 'react-native'

function AppbarHeader({
  safe,
  style,
  children,
  ...rest
}: {
  safe: { top: number; bottom: number; left: number; right: number }
  style: StyleProp<any>
  children: ReactChildren
}) {
  return (
    <Appbar
      style={[
        {
          height: 56 + safe.top,
          paddingTop: safe.top,
          paddingLeft: safe.left + 6,
          paddingRight: safe.right + 6,
          backgroundColor: '#FFF',
        },
        style,
      ]}
      {...rest}
    >
      {children}
    </Appbar>
  )
}

export default safeAreaHOC(AppbarHeader)
