import { Platform } from 'react-native'

const fonts = Platform.select({
  web: {
    regular: {
      fontFamily: 'Nunito, sans-serif',
      fontWeight: '500',
    },
    medium: {
      fontFamily: 'Nunito, sans-serif',
      fontWeight: '600',
    },
    light: {
      fontFamily: 'Nunito, sans-serif',
      fontWeight: '300',
    },
    thin: {
      fontFamily: 'Nunito, sans-serif',
      fontWeight: '100',
    },
  },
  ios: {
    regular: {
      fontFamily: 'Montserrat',
      fontWeight: '500',
    },
    medium: {
      fontFamily: 'Montserrat',
      fontWeight: '600',
    },
    light: {
      fontFamily: 'Montserrat',
      fontWeight: '400',
    },
    thin: {
      fontFamily: 'Montserrat',
      fontWeight: '100',
    },
  },
  default: {
    regular: {
      fontFamily: 'Nunito, sans-serif',
    },
    medium: {
      fontFamily: 'Nunito, sans-serif',
      // fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Nunito, sans-serif',
      // fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'Nunito, sans-serif',
      // fontWeight: 'normal',
    },
  },
})

export default fonts
