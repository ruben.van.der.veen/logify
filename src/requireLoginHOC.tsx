import { useSelector } from 'react-redux'
import { isEmpty, isLoaded } from 'react-redux-firebase'

export default function requireLoginHOC(
  WrappedComponent: any,
  pushAuth: () => any
) {
  // ...and returns another component...
  return function (props: any) {
    const auth = useSelector((state: any) => state.firebase.auth)
    console.log(auth)
    if (isLoaded(auth)) {
      if (!isEmpty(auth)) {
        return <WrappedComponent {...props} />
      } else {
        pushAuth()
      }
    }
    return null
  }
}
