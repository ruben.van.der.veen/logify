import HomeScreen from './Home/Home'
import LoginScreen from './Auth/Auth'
import AvailabilityScreen from './Availability/Availability'
import Dashboard from './Dashboard/Dashboard'
import requireLoginHOC from './requireLoginHOC'
import { switchMenu } from './Navigation.web'
import MyDashboard from './Dashboard/MyDashboard'
import Account from './Account/Account'

export const HOME_SCREEN_ROUTE = `/`
export const AUTH_SCREEN_ROUTE = '/auth'
export const AVAILABILITY_SCREEN_ROUTE = '/availability'
export const DASHBOARD_SCREEN_ROUTE = '/dashboard'
export const MY_DASHBOARD_SCREEN_ROUTE = '/my-jobs'
export const MY_ACCOUNT_SCREEN_ROUTE = '/account'

const requiresLogin = (component: any) => {
  return requireLoginHOC(component, () => {
    switchMenu('/auth')
  })
}

export type RouteType = {
  path: string
  pathOnWeb?: string
  exact?: boolean
  component: any
  deepLinkWithoutLogin?: boolean
}
export const routeObjectToWeb = (lr: RouteType) => ({
  key: lr.path,
  path: lr.pathOnWeb || lr.path,
  exact: lr.exact,
  component: lr.component,
})

export const listRoutes: RouteType[] = [
  {
    path: AUTH_SCREEN_ROUTE,
    exact: true,
    component: LoginScreen,
  },
  {
    path: HOME_SCREEN_ROUTE,
    exact: true,
    pathOnWeb: `${HOME_SCREEN_ROUTE}`,
    component: requiresLogin(HomeScreen),
  },
  {
    path: AVAILABILITY_SCREEN_ROUTE,
    exact: true,
    pathOnWeb: `${AVAILABILITY_SCREEN_ROUTE}`,
    component: requiresLogin(AvailabilityScreen),
  },
  {
    path: DASHBOARD_SCREEN_ROUTE,
    exact: true,
    pathOnWeb: `${DASHBOARD_SCREEN_ROUTE}`,
    component: requiresLogin(Dashboard),
  },
  {
    path: MY_DASHBOARD_SCREEN_ROUTE,
    exact: true,
    pathOnWeb: `${MY_DASHBOARD_SCREEN_ROUTE}`,
    component: requiresLogin(MyDashboard),
  },
  {
    path: MY_ACCOUNT_SCREEN_ROUTE,
    exact: true,
    pathOnWeb: `${MY_ACCOUNT_SCREEN_ROUTE}`,
    component: requiresLogin(Account),
  },
]

export const detailRoutes: RouteType[] = []

export const allRoutes = detailRoutes.concat(listRoutes)
