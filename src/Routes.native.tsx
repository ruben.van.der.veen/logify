import { Navigation } from 'react-native-navigation'
import { allRoutes } from './Routes.config'
import { HOC } from './App'

export function registerScreens() {
  allRoutes.map((route) => {
    Navigation.registerComponent(route.path, () => HOC(route.component))
  })
}
