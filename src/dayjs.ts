import 'dayjs/locale/en' // load on demand
import dayjs, { Dayjs } from 'dayjs'
import cpf from 'dayjs/plugin/customParseFormat'
import lf from 'dayjs/plugin/localizedFormat'
import isToday from 'dayjs/plugin/isToday'
import isTomorrow from 'dayjs/plugin/isTomorrow'

dayjs.extend(cpf)
dayjs.extend(lf)
dayjs.extend(isToday)
dayjs.extend(isTomorrow)

// creates a day name from a date string or timestamp
export const getFriendlyDate = (
  date: Dayjs,
  withTime = false,
  reversed = false
): string => {
  // var now = dayjs()
  const createdAt = dayjs(date)

  // if (dayjs(date).isSame(now, 'year')) {
  //   return createdAt.locale('nl').format(withTime ? 'D MMMM H:mm' : 'D MMMM')
  // }
  if (createdAt.isToday()) {
    return reversed
      ? `Today ${createdAt
          .locale('nl')
          .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY')}`
      : `${createdAt
          .locale('nl')
          .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY')} Today`
  }
  if (createdAt.isTomorrow()) {
    return `${createdAt
      .locale('nl')
      .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY')} Tomorrow`
  }
  return `${
    createdAt
      .locale('nl')
      .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY dddd')
      .slice(0, 11) +
    createdAt
      .locale('nl')
      .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY dddd')
      .charAt(11)
      .toUpperCase() +
    createdAt
      .locale('nl')
      .format(withTime ? 'DD-MM-YYYY H:mm' : 'DD-MM-YYYY dddd')
      .slice(12)
  }`
}
export const getFriendlyShortDate = (date: string | number | Dayjs): string => {
  var now = dayjs()
  const createdAt = dayjs(date, 'YYYY-MM-DD HH:mm:ss')

  if (dayjs(date).isSame(now, 'day')) {
    return createdAt.locale('nl').format('HH:ss')
  } else if (dayjs(date).isSame(now, 'week')) {
    return createdAt.locale('nl').format('dd')
  } else if (dayjs(date).isSame(now, 'month')) {
    return createdAt.locale('nl').format('D MMMM')
  } else if (dayjs(date).isSame(now, 'year')) {
    return createdAt.locale('nl').format('D MMMM')
  }
  return createdAt.locale('nl').format('D MMMM YYYY')
}

export const getApplicationDate = (date: Dayjs): string => {
  var now = dayjs()
  const createdAt = dayjs(date, 'YYYY-MM-DD HH:mm:ss')

  if (dayjs(date).isSame(now, 'day')) {
    return 'Vandaag'
  } else if (dayjs(date).isSame(now, 'week')) {
    return createdAt.locale('nl').format('dddd')
  } else if (dayjs(date).isSame(now, 'month')) {
    return createdAt.locale('nl').format('D MMMM')
  } else if (dayjs(date).isSame(now, 'year')) {
    return createdAt.locale('nl').format('D MMMM')
  }
  return createdAt.locale('nl').format('D MMMM YYYY')
}

export const fromTimestamp = (timestamp: number) => {
  return dayjs.unix(timestamp)
}

export const setLocale = (locale: any) => {
  dayjs.locale(locale)
}

export default dayjs
