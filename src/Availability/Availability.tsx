import React, { useEffect } from 'react'
import Resizer from '../components/Resizer'
import { ScrollView, StyleSheet, View } from 'react-native'
import safeAreaHOC from '../WidthAndHeight/safeAreaHOC'
import { Button, FAB, Modal, Text, Title, useTheme } from 'react-native-paper'
import { DatePickerModal } from 'react-native-paper-dates'
import { getFriendlyDate } from '../dayjs'
import widthAndHeightHOC from '../WidthAndHeight/widthAndHeightHOC'
import { useDispatch, useSelector } from 'react-redux'
import * as aN from '../Notifications/actions'
import { isLoaded, useFirestore } from 'react-redux-firebase'
import dayjs from 'dayjs'
// type HomeProps = {
//   componentId: string
//   safe: any
// }

const styles = StyleSheet.create({
  titleStyle: {},
  container: {
    flexDirection: 'column',
    // height: 400,
    flex: 1,
    maxHeight: 'calc(100vh-120px)',
    flexGrow: 200,
  },
  button: {
    position: 'absolute',
    bottom: 16,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
  },
  fab: {
    position: 'absolute',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },

    elevation: 5,

    shadowOpacity: 0.05,
    shadowRadius: 4.65,
    // backgroundColor: theme.palette.primary.main,
  },

  fabPusher: {
    paddingBottom: 16 + 36 + 16,
  },
})

function Availability({ safe, isMedium }: { safe: any; isMedium: boolean }) {
  const dispatch = useDispatch()
  const firestore = useFirestore()
  const { uid } = useSelector((state: any) => state.firebase.auth)

  const { availability } = useSelector((state: any) => state.firestore.ordered)
  const list = ((availability && availability) || []).map((it: any) => {
    return {
      ...it,
      dates: it.dates.map((date: any) => {
        return new Date(date.seconds * 1000)
      }),
    }
  })
  const [dates, setDates] = React.useState(
    (list && list.length > 0 && list[0].dates) || []
  )
  const [visible, setVisible] = React.useState(false)
  const [pusherVisible, setPusherVisible] = React.useState(false)
  const [pusherSetVisible, setPusherSetVisible] = React.useState(false)
  useEffect(() => {
    if (list && list.length > 0 && list[0].dates && dates.length === 0) {
      setDates(list && list.length > 0 && list[0].dates)
    }
    if (list && list.length > 0 && list[0].dates) {
      if (
        !list[0].dates.some((date: any) => dayjs(date).isToday()) &&
        !pusherSetVisible
      ) {
        setPusherVisible(true)
        setPusherSetVisible(true)
      }
    } else if (
      !list ||
      (list.length === 0 && isLoaded(availability) && !pusherSetVisible)
    ) {
      setPusherVisible(true)
      setPusherSetVisible(true)
    }
  }, [list, dates.length, pusherSetVisible, availability])
  const setFree = (dates: any) => {
    console.log(dates)
    setVisible(false)
    setPusherVisible(false)
    setPusherSetVisible(true)
    setDates(dates.dates)
    firestore.collection('availability').doc(uid).set(
      {
        dates: dates.dates,
      },
      { merge: true }
    )

    dispatch(
      aN.setMessages(['OK! We have successfully saved your availability.'])
    )
  }
  const theme = useTheme()
  return [
    <ScrollView style={{ margin: 16, height: '100%' }}>
      <Resizer>
        <View>
          <View style={styles.titleStyle}>
            <Title>Availability</Title>
          </View>
          <DatePickerModal
            mode={'multiple'}
            onConfirm={setFree}
            onDismiss={() => setVisible(false)}
            visible={visible}
            dates={dates}
            validRange={{
              startDate: new Date(), // optional
            }}
          />
          <ScrollView style={styles.container}>
            {(dates || [])
              .filter((date: any) => {
                const yesterday = dayjs(new Date())
                  .set('hour', 23)
                  .set('minute', 59)
                  .set('second', 59)
                  .add(-1, 'day')

                return !dayjs(date).isBefore(yesterday)
              })
              .map((date: any) => {
                return <Text>{getFriendlyDate(date)}</Text>
              })}
          </ScrollView>
        </View>
      </Resizer>
    </ScrollView>,
    <FAB
      onPress={() => setVisible(true)}
      icon={'pencil'}
      color={'#FFF'}
      style={{
        margin: 5,
        backgroundColor: theme.colors.primary,
        bottom: 16 + safe.bottom,
        right: 16 + safe.right,

        position: 'absolute',
        // borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 3,
        },

        elevation: 5,

        shadowOpacity: 0.05,
        shadowRadius: 4.65,
      }}
    />,
    <Modal
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      contentContainerStyle={{
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
      }}
      visible={pusherVisible}
      onDismiss={() => setPusherVisible(false)}
    >
      <Title>Are you available today?</Title>
      <View style={{ flexDirection: 'row', flexGrow: 1, marginTop: 50 }}>
        <Button
          onPress={() => setFree({ dates: [...dates, new Date()] })}
          mode={'contained'}
          style={{ flex: 1, marginRight: 5 }}
        >
          <Text style={{ color: '#FFF' }}>Yes</Text>
        </Button>
        <Button
          onPress={() => setPusherVisible(false)}
          mode={'contained'}
          color={'#EFEFEF'}
          style={{ flex: 1, marginLeft: 5 }}
        >
          <Text style={{ color: '#000' }}>No</Text>
        </Button>
      </View>
    </Modal>,
  ]
}

export default widthAndHeightHOC(safeAreaHOC(Availability))
