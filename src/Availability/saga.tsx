import actions from './actions'
import saga from '../GeneralListFunctions/sagaFactory'

const sagaConst = () =>
  saga(
    actions,
    () => 'availability/1',
    (state) => state.availability
  )
export default sagaConst
