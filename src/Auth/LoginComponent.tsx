import React, { useState } from 'react'
import { View } from 'react-native'
import { Button, Snackbar, TextInput } from 'react-native-paper'
import { colors, styles } from './styles'
import { useHistory } from 'react-router'
import { useFirebase } from 'react-redux-firebase'

export function LoginComponent({
  hasError,
}: {
  hasError: (errorMessage: string) => any
}) {
  const firebase = useFirebase()
  const history = useHistory()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const __doLogin = () => {
    firebase
      .login({ email, password })
      .then(() => {
        history.push('/')
      })
      .catch((err) => showError(err.message))
  }

  const showError = (message: string) => {
    setErrorMessage(message)
    setTimeout(() => {
      setErrorMessage('')
    }, 3000)
  }

  return (
    <View>
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        label="username"
        autoCompleteType={'email'}
        textContentType={'emailAddress'}
        onChangeText={(value) => setEmail(value)}
        defaultValue={email}
      />
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        label="password"
        secureTextEntry={true}
        autoCompleteType={'password'}
        textContentType={'password'}
        onChangeText={(value) => setPassword(value)}
        defaultValue={password}
      />
      <Button
        style={styles.button}
        theme={{ colors: { primary: colors.white, surface: colors.secondary } }}
        onPress={__doLogin}
      >
        let's go
      </Button>
      <Snackbar
        visible={errorMessage !== ''}
        onDismiss={() => setErrorMessage('')}
      >
        {errorMessage}
      </Snackbar>
    </View>
  )
}
