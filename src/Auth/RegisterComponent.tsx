import { useState } from 'react'
import { ScrollView, View } from 'react-native'
import { Button, RadioButton, Text, TextInput, Title } from 'react-native-paper'
import { useFirebase } from 'react-redux-firebase'
import { colors, styles } from './styles'

function checkFields(arr: string[]) {
  arr.forEach((it: string) => {
    if (!(it && it.length > 0)) {
      return false
    }
  })
  return true
}

export function RegisterComponent(props: any) {
  const firebase = useFirebase()
  const [firstname, setFirstname] = useState<string>('')
  const [lastname, setLastname] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [confirmPassword, setConfirmPassword] = useState<string>('')
  const [role, setRole] = useState<string>('')

  const __doRegister = () => {
    if (
      checkFields([firstname, lastname, email, password, confirmPassword, role])
    ) {
      firebase
        .createUser({ email, password, signIn: false }, { role })
        .then(props.registered)
        .catch((err) => showError(err.message))
    }
  }

  const showError = (message: string) => {
    props.hasError(message)
  }

  return (
    <ScrollView>
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        label="firstname"
        textContentType={'name'}
        onChangeText={(value) => setFirstname(value)}
        defaultValue={firstname}
      />
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        label="lastname"
        onChangeText={(value) => setLastname(value)}
        defaultValue={lastname}
      />
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        label="email"
        textContentType={'emailAddress'}
        onChangeText={(value) => setEmail(value)}
        defaultValue={email}
      />
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        secureTextEntry={true}
        label="password"
        textContentType={'newPassword'}
        onChangeText={(value) => setPassword(value)}
        defaultValue={password}
      />
      <TextInput
        mode="outlined"
        theme={{ colors: { primary: colors.secondary } }}
        style={styles.input}
        secureTextEntry={true}
        label="confirm password"
        textContentType={'newPassword'}
        onChangeText={(value) => setConfirmPassword(value)}
        defaultValue={confirmPassword}
      />
      <View style={{ marginBottom: 12 }}>
        <Title>Role</Title>
        <RadioButton.Group
          value={role}
          onValueChange={(val: string) => setRole(val)}
        >
          <View style={{ flexDirection: 'row' }}>
            <RadioButton value={'supplier'} />
            <Text style={{ paddingTop: 8 }}>Supplier</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <RadioButton value={'transporter'} />
            <Text style={{ paddingTop: 8 }}>Transporter</Text>
          </View>
        </RadioButton.Group>
      </View>
      <Button
        style={styles.button}
        theme={{ colors: { primary: colors.white, surface: colors.secondary } }}
        onPress={() => __doRegister()}
      >
        let's go
      </Button>
    </ScrollView>
  )
}
