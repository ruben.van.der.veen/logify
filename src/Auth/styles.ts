import { StyleSheet } from 'react-native'

export const colors = {
  primary: '#58B259',
  secondary: '#494A4A',
  black: '#010e01',
  white: '#fff',
}

export const styles = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: colors.primary,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  header: {
    width: '100%',
    justifyContent: 'center',
  },
  logo: {
    height: '200px',
    resizeMode: 'contain',
  },
  tabContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    height: '100%',
  },
  tabContent: {
    flex: 1,
    padding: 16,
    height: '100%',
    width: '100%',
    maxWidth: 500,
  },
  tabWrapper: {
    display: 'flex',
    flexDirection: 'row',
    padding: 16,
    width: '100%',
    maxWidth: 500,
  },
  tabTitle: {
    lineHeight: 50,
    color: colors.white,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
  },
  tab: {
    width: '50%',
    height: 50,
  },
  tabActive: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    div: { color: colors.secondary },
  },
  input: {
    backgroundColor: colors.primary,
    marginBottom: 16,
  },
  button: {
    height: 50,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: colors.secondary,
  },
})
