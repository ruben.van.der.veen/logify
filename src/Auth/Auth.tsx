import { useState } from 'react'
import { Image, Pressable, ScrollView, View } from 'react-native'
import { Snackbar, Text } from 'react-native-paper'
import logo from '../vectors/logo-white.png'
import { styles } from './styles'
import { LoginComponent } from './LoginComponent'
import { RegisterComponent } from './RegisterComponent'

const Authentication = () => {
  enum authTab {
    Login,
    Register,
  }
  const [activeTab, setActiveTab] = useState(authTab.Login)
  const [errorMessage, setErrorMessage] = useState('')

  const loginTabStyle = () => {
    return activeTab === authTab.Login
      ? [styles.tab, styles.tabActive]
      : styles.tab
  }

  const registerTabStyle = () => {
    return activeTab === authTab.Register
      ? [styles.tab, styles.tabActive]
      : styles.tab
  }

  const handleRegistered = () => {
    setActiveTab(authTab.Login)
  }

  const handleHasError = (errorMessage: string) => {
    showError(errorMessage)
  }

  const showError = (message: string) => {
    setErrorMessage(message)
    setTimeout(() => {
      setErrorMessage('')
    }, 3000)
  }

  // Draw View
  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Image style={styles.logo} source={{ uri: logo }} />
      </View>
      <View style={styles.tabWrapper}>
        <Pressable
          onPress={() => setActiveTab(authTab.Login)}
          style={loginTabStyle()}
        >
          <Text style={styles.tabTitle}>login</Text>
        </Pressable>
        <Pressable
          onPress={() => setActiveTab(authTab.Register)}
          style={registerTabStyle()}
        >
          <Text style={styles.tabTitle}>register</Text>
        </Pressable>
      </View>
      <ScrollView
        style={styles.tabContainer}
        contentContainerStyle={{
          alignItems: 'center',
        }}
      >
        <ScrollView style={styles.tabContent}>
          {activeTab === authTab.Login ? (
            <LoginComponent
              hasError={(errorMessage) => handleHasError(errorMessage)}
            />
          ) : (
            <RegisterComponent
              hasError={(errorMessage: string) => handleHasError(errorMessage)}
              registered={handleRegistered}
            />
          )}
        </ScrollView>
      </ScrollView>
      <Snackbar
        style={{ maxWidth: 500, marginHorizontal: 'auto', width: '100%' }}
        onDismiss={() => null}
        visible={errorMessage !== ''}
      >
        {errorMessage}
      </Snackbar>
    </View>
  )
}

export default Authentication
