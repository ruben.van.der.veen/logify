import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Snackbar } from 'react-native-paper'
import { StyleSheet, Platform } from 'react-native'
import { RootState } from '../reducers'
const styles = StyleSheet.create({
  root: {
    zIndex: 1000,
    position: 'absolute',
    bottom: 0,
    width: undefined,
    right: 0,
    left: 0,
  },
  rootPaddingBottom: {
    marginBottom: 55,
  },
})

class NotificationContainer extends Component<any, any> {
  state = {
    visible: false,
  }
  componentDidUpdate(prevProps: { messages: any }) {
    const { messages } = this.props

    if (prevProps.messages !== messages && messages && messages.length > 0) {
      this.setState({
        visible: true,
      })
    }
  }
  _dismiss = () => {
    this.setState({ visible: false })
  }
  render() {
    const { messages, isMedium } = this.props
    let paddingBottom = false
    if (!isMedium && Platform.OS === 'web') {
      paddingBottom = true
    }

    return (
      <Snackbar
        visible={this.state.visible}
        onDismiss={this._dismiss}
        theme={{
          colors: {
            accent: '#FFFFFF',
          },
        }}
        action={{
          label: 'Ok',
          onPress: this._dismiss,
        }}
        style={[styles.root, paddingBottom && styles.rootPaddingBottom]}
      >
        {messages.map(
          (message: string, i: number) => `${i === 0 ? '' : '\n'}${message}`
        )}
      </Snackbar>
    )
  }
}

export default connect((state: RootState) => ({
  messages: state.notification.messages,
}))(NotificationContainer)
