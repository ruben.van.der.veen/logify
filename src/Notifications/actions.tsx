import { actionCreator as ac } from '../helpers/actionCreator'

const ns = (type: string) => `NOTIFICATION_${type}`

export const SET_MESSAGES = ns('SET_MESSAGES')
export const setMessages = ac(SET_MESSAGES)
