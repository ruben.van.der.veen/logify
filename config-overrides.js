// config-overrides.js
const {
  addWebpackAlias,
  babelInclude,
  fixBabelImports,
  override,
  disableChunk,
} = require('customize-cra')

const path = require('path')

module.exports = override(
  fixBabelImports('module-resolver', {
    alias: {
      '^react-native$': 'react-native-web',
    },
  }),
  addWebpackAlias({
    'react-native': 'react-native-web',
    // here you can add extra packages
  }),
  disableChunk(),

  babelInclude([
    path.resolve('src'),
    path.resolve('app.json'),

    // any react-native modules you need babel to compile
    path.resolve('./node_modules/react-native-vector-icons'),
  ])
)
